import {Header, Title, Button, Icon} from 'native-base';
import React from 'react';

class AppHeader extends React.Component {

    constructor(props) {
        super(props);
    }

    renderTitle(title) {
       return <Title>{title}</Title>;
    };

    renderLogOutBtn(logOutEvent) {
        return <Button transparent><Icon name='ios-exit-outline' onPress={logOutEvent}/></Button>
    };

    renderLeftBtn(leftBtnEvent){
        return <Button transparent><Icon name='ios-arrow-back' onPress={leftBtnEvent}/></Button>;
    };

    renderRightBtn(rightBtnEvent) {
        return <Button transparent><Icon name='ios-arrow-forward' onPress={rightBtnEvent}/></Button>;
    };

    initAppHeaderElements(leftArrow, rightArrow, logout, title){
        return <Header>
            {this.renderTitle(title)}
            {logout ? this.renderLogOutBtn(logout): null}
            {leftArrow ?  this.renderLeftBtn(leftArrow): null}
            {rightArrow ?  this.renderRightBtn(rightArrow): null}
        </Header>;
    };

    initAppHeaderWithOutElements(title){
        return <Header><Title>{title}</Title></Header>
    }

    render() {
        const {name, rightArrow, leftArrow, logout,  main} = this.props;
        if (main) {
            return (this.initAppHeaderWithOutElements(name));
        } else {
            return (this.initAppHeaderElements(leftArrow, rightArrow, logout, name));
        }
    }
}

export default AppHeader;