import * as actionTypes from '../actionTypes';

export const fetchMetricRequest = (data) => {
    return {type: actionTypes.FETCH_METRIC, payload: data};
};