import {fetchMetricRequest} from '../metric/metricDispatchActions';
import {beginAjaxCall, ajaxCallSuccess, revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';
import {METRIC_JVM_URL} from '../../constants/urlConstants';
import {AsyncStorage} from 'react-native';
import {prepareBarChartMetricData} from '../../utils/ChartsUtil';

export const fetchMetric = (metricParam) => {
    return ((dispatch) => {
        dispatch(revertAjaxStatusState());
        dispatch(beginAjaxCall());
        AjaxObservable.ajaxGet(METRIC_JVM_URL)
            .map(data => {
                let outData = data.response.gauges;
                return prepareBarChartMetricData(metricParam, outData);
            })
            .catch((err) => {
                return Observable.empty();
            })
            .subscribe(data => {
                if (data.length != 0) {
                    console.log(typeof  data);
                    dispatch(ajaxCallSuccess());
                    dispatch(fetchMetricRequest(data));
                }
            });
    });
};
