import {authUserSuccess, authUserFailure} from '../auth/authDispatchActions';
import {beginAjaxCall, ajaxCallSuccess,  revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';
import {BASE_URL} from '../../constants/urlConstants';
import {AsyncStorage} from 'react-native';

export const signIn = (userName, userPassword) => {
    let jsonParam = JSON.stringify({'username': userName, 'password': userPassword});
    return (dispatch) => {
        dispatch(revertAjaxStatusState());
        dispatch(beginAjaxCall());
        AjaxObservable.ajaxPost(`${BASE_URL}/auth`, jsonParam, {'Content-Type': 'application/json'})
            .map(data => data.response)
            .catch((err) => {
                dispatch(authUserFailure());
                return Observable.empty();
            })
            .subscribe(data => {
                dispatch(ajaxCallSuccess());
                dispatch(authUserSuccess());
                AsyncStorage.setItem('token', data.token);
            });
    };
};

export const signOut = () => {
    return (dispatch) => {
        dispatch(revertAjaxStatusState());
        dispatch(authUserFailure());
        AsyncStorage.removeItem('token');
    }
};

