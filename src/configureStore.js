/* eslint global-require: 0 */

import Immutable from 'immutable';
import { Platform } from 'react-native';
import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducer from './reducers';
import {persistStore, autoRehydrate} from 'redux-persist';
import { AsyncStorage } from 'react-native';

const middlewares = [thunk];

let enhancer;
let updateStore = f => f;
//if dev mode -> we apply redux dev-tools, else -> we apply only redux thunk
if (__DEV__) {
  /* eslint import/no-extraneous-dependencies: ["error", {"devDependencies": true}] */
  const installDevTools = require('immutable-devtools');
  const devTools = global.reduxNativeDevTools || require('remote-redux-devtools');

  installDevTools(Immutable);
  updateStore = devTools.updateStore || updateStore;

  enhancer = compose(
    applyMiddleware(...middlewares),
    // apply rehydrate store
    autoRehydrate(),
    devTools({
      name: Platform.OS,
      ...require('../package.json').remotedev
    })
  );
} else {
  enhancer = compose(
      applyMiddleware(...middlewares),
      autoRehydrate());
}

export default function configureStore(initialState) {
  const store = createStore(reducer, initialState, enhancer);
  // persist store intp AsyncStorage while application is starting...
  persistStore(store, {storage: AsyncStorage}),
  updateStore(store);
  if (module.hot) {
    module.hot.accept(() => {
      store.replaceReducer(require('./reducers').default);
    });
  }
  return store;
}
