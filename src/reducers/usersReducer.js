import * as types from '../actions/actionTypes';

const initUsersState = {
  payload: [],
  count: 0
};

export default function users(state = initUsersState, action) {
    switch (action.type) {
        case types.FETCH_USERS:
            return {...state, payload: action.payload};
        case types.FETCH_USERS_BY_NAME:
            return {...state, payload: action.payload}
        case types.FETCH_USER:
            return {...state, payload: action.payload};
        case types.FETCH_COUNT_USERS:
            return {...state, count: action.count};
        case types.REMOVE_USER:
            return {...state, payload: action.payload};
        default:
            return state;
    }
}
