import {combineReducers} from 'redux';
import routes from './routes';
import users from './usersReducer';
import ajax from './ajaxReducer';
import auth from './authReducer';
import metric from './metricReducer';

export default combineReducers({
    routes,
    users,
    ajax,
    auth,
    metric
});
