import React, {Component, PropTypes} from "react";
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Container, Content, Card, CardItem, List, ListItem, InputGroup, Input, Icon, Button, Grid, Row} from "native-base";
import * as authActions from '../actions/auth/authActions';
import AppHeader from '../components/AppHeader';
import {MainTitle} from '../common/AppParams';
import appTheme from '../common/AppStyle';

class LoginScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: 'admin',
            password: 'admin'
        };
    }

    toNextScreen() {
        const {navigate} = this.props;
        navigate({
            type: 'push',
            key: 'admin'
        });
    };

    handleLogin = () => {
        const login = this.state.login;
        const password = this.state.password;
        this.props.authActions.signIn(login, password);
        if(this.props.auth.authenticated){
            this.toNextScreen();
        }
    };

    renderErrorInputGroup(errorMsg) {
        return <InputGroup iconRight error>
            <Icon name='ios-close-circle' style={{color:'red'}}/>
            <Input placeholder={errorMsg}/>
        </InputGroup>
    };

    renderLoginPanel(){
        return <Card>
            <CardItem>
                <List>
                    <ListItem>
                            <InputGroup>
                                <Icon name='ios-person'/>
                                <Input placeholder='login' onChangeText={login => this.setState({login})}/>
                            </InputGroup>
                    </ListItem>
                    <ListItem>
                            <InputGroup>
                                <Icon name='ios-unlock'/>
                                <Input placeholder='password' secureTextEntry={true} onChangeText={password => this.setState({password})}/>
                            </InputGroup>
                    </ListItem>
                </List>
            </CardItem>
            <CardItem>
                <Button block rounded primary onPress={this.handleLogin}>LOGIN</Button>
            </CardItem>
        </Card>
    };

    render() {
        return (
            <Container>
                <Content theme={appTheme}>
                    <AppHeader name={MainTitle} main/>
                    <Grid>
                        <Row style={{ backgroundColor: 'aliceblue', height: 200 }}>
                        </Row>
                        <Row style={{ backgroundColor: 'white', height: 200 }}>
                            {this.renderLoginPanel()}
                        </Row>
                        <Row style={{ backgroundColor: 'aliceblue', height: 2 }}/>
                    </Grid>
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.auth
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        authActions: bindActionCreators(authActions, dispatch)
    };
};

LoginScreen.propTypes = {
    navigate: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    authActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);

