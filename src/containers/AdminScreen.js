import React, {Component, PropTypes} from 'react';
import {Container, Content, List, ListItem, Text, Icon, Badge} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as authActions from '../actions/auth/authActions';
import * as userActions  from '../actions/users/userActions';
import AppHeader from '../components/AppHeader';
import {AdminScreenTitle} from '../common/AppParams';

class AdminScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    componentWillMount(){
        this.handleUsersCount();
    }

    handleLogout = () => {
        this.props.authActions.signOut();
        const {navigate} = this.props;
        navigate({type: 'pop'});
    };



    handleUsersScreen = () => {
        const {navigate} = this.props;
        navigate({
            type: 'push',
            key: 'users'
        });
    };

    handleUsersCount = () => {
        this.props.userActions.fetchUsersCount();
    };

    handleMetricScreen = () => {
        const {navigate} = this.props;
        navigate({
            type: 'push',
            key: 'metric'
        });
    };

    handleOpenTokRoomScreen = () => {
        const {navigate} = this.props;
        navigate({
            type: 'push',
            key: 'opentok'
        });
    };

    render() {
        return (
            <Container>
                <Content>
                    <AppHeader name={AdminScreenTitle} logout={this.handleLogout}/>
                    <List>
                        <ListItem itemDivider >
                            <Icon name='ios-people'/>
                            <Text>System users monitoring</Text>
                        </ListItem>
                        <ListItem iconLeft button onPress={this.handleUsersScreen}>
                            <Icon name='ios-contacts' />
                            <Text>Users list</Text>
                            <Badge>{this.props.users.count}</Badge>
                        </ListItem>
                        <ListItem iconLeft>
                            <Icon name='ios-pie-outline' />
                            <Text>Users statistic</Text>
                        </ListItem>
                        <ListItem itemDivider>
                            <Icon name='ios-construct'/>
                            <Text>System info</Text>
                        </ListItem>
                        <ListItem iconLeft onPress={this.handleMetricScreen}>
                            <Icon name='ios-speedometer' />
                            <Text>Metrics</Text>
                            <Text note>JVM metric etc.</Text>
                        </ListItem>
                        <ListItem itemDivider>
                            <Icon name='ios-film'/>
                            <Text>Video</Text>
                        </ListItem>
                        <ListItem iconLeft onPress={this.handleOpenTokRoomScreen}>
                            <Icon name='ios-videocam' />
                            <Text>OpenTok</Text>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        );
    }
}

function mapStateToProps(state) {
    return {
        auth: state.auth,
        ajax: state.ajax,
        users: state.users
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        authActions: bindActionCreators(authActions, dispatch),
        userActions: bindActionCreators(userActions, dispatch)
    };
};

AdminScreen.propTypes = {
    navigate: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    ajax: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(AdminScreen);