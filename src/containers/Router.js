import React, { Component, PropTypes } from 'react';
import { NavigationExperimental } from 'react-native';
import { connect } from 'react-redux';
import Login from './LoginScreen';
import Users from './UsersScreen';
import Metric from './MetricScreen';
import Admin from './AdminScreen';
import UserDetail from './UserDetailScreeen';
import OpenTokRoom from './OpenTokRoom';

const { CardStack } = NavigationExperimental;

@connect(
  state => state,
  dispatch => ({ dispatch })
)
export default class Router extends Component {
  static propTypes = {
    routes: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  handleNavigation = action => {
    this.props.dispatch(action);
  };

  renderScene = props => {
    switch (props.scene.key) {
        case 'scene_login':
            return <Login navigate={this.handleNavigation}/>;
        case 'scene_admin':
            return <Admin navigate={this.handleNavigation}/>;
        case 'scene_opentok':
            return <OpenTokRoom navigate={this.handleNavigation}/>
        case 'scene_users':
            return <Users navigate={this.handleNavigation}/>;
        case 'scene_user_detail':
            return <UserDetail navigate={this.handleNavigation}/>;
        case 'scene_metric':
            return <Metric navigate={this.handleNavigation}/>;
        default:
            return null;
    }
  };

  render() {
    return (
      <CardStack
        direction="horizontal"
        navigationState={this.props.routes}
        renderScene={this.renderScene}/>
    );
  }
}
