import React, {Component, PropTypes} from 'react';
import {Container, Content, Button, Text, Spinner} from 'native-base';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as metricActions from '../actions/metric/metricActions';
import {Bar} from 'react-native-pathjs-charts'
import sampleData from '../common/fakeChartData';
import AppHeader from '../components/AppHeader';
import {MetricScreenTitle} from '../common/AppParams';

class MetricScreen extends React.Component {

    constructor(props) {
        super(props);
    }

    handleBack = () => {
        const {navigate} = this.props;
        navigate({type: 'pop'});
    };

    getThreadMetricData = () =>{
      this.props.metricActions.fetchMetric("thread");
    };

    getMemoryMetricData = () => {
      this.props.metricActions.fetchMetric("memory");
    };

    prepareBarChartData(data){
        if(this.props.ajax.loading){
            return <Spinner/>;
        }else{
            return <Bar data={data} options={sampleData.bar.options} accessorKey='v'/>;
        }
    }

    render() {
        const barDara = [this.props.metric.payload];
        return (
            <Container>
                <Content>
                    <AppHeader name={MetricScreenTitle} leftArrow={this.handleBack}/>
                    <Button style={{flex: 1}} block info onPress={this.getThreadMetricData}>Get thrad metric</Button>
                    <Button style={{flex: 1}} block success onPress={this.getMemoryMetricData}>Get memory metric</Button>
                    {this.props.metric.payload.length == 0 ?  <Text>no data...</Text>: this.prepareBarChartData(barDara)}
                </Content>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        metric: state.metric,
        ajax: state.ajax
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        metricActions: bindActionCreators(metricActions, dispatch)
    };
};

MetricScreen.propTypes = {
    navigate: PropTypes.func.isRequired,
    metric: PropTypes.object.isRequired,
    ajax: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(MetricScreen);