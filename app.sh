#!/bin/bash
echo "==========================================="
echo "Welcome to react-native boilerplate project!"
echo "==========================================="

# init variables: javascript file with ip configuration
config=AppIPConfig.js;
tmpFile=tmpFile;
ipStr='';
ipRegexp="(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)"
ip='';

# Detect the platform
OS="`uname`"
case "$OSTYPE" in
  solaris*)
  echo "SOLARIS"
  ;;
  darwin*)
  echo "OSX"
  ;;
  linux*)
  echo "LINUX"
   ip=$('ifconfig | grep "inet " | cut -f2 -d')
  ;;
  bsd*)
  echo "BSD"
  ;;
  *)
  echo "windows: $OSTYPE"
  #using netsh get ipv4 adress
  $(cd src)
  ipStr=$(netsh interface ip show address "Ethernet" | findstr "IP Address" | findstr "IP")
  ;;
esac


#write javascript IP constant into file
echo "$ipStr" >> $tmpFile

# extract ip adress from file using grep command and regular expression
ip=$(grep -E -o $ipRegexp  $tmpFile)
echo "Exported ipv4: $ip"

#remove temporary file
rm $tmpFile

#if temporary file exists - then remove it
if [ -f $config ] ; then
rm $config
fi

#write javascript IP constant into file
echo "export const IP = '$ip';" >> $config
echo "==========================================="
echo "DONE!"
echo "==========================================="
